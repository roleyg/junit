public class Junit {

    // Variables
    static int result;
    static String text;

    // Multiple 2 integers
    public static int junits(int a, int b){

        result =  a * b;
        return result;
    }

    // Concatenate strings
    public static String junitsText(String a, String b){
        text = a.concat(b);
        return text;
    }

    public static boolean trueOrfalse(int a){
        System.out.println("Is one always greater than zero?");
        boolean yes = a > 0;
        return yes;
    }
}